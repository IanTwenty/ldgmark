<!--
SPDX-FileCopyrightText: 2022-4 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

Recover from vim crashes

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/ldgmark/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 0.0.1 - 2024-01-18

### Changed

- No longer use take a journal name in config. We want to use ldgmark as a
  formatter and we have no access to the journal name as we must process from
  stdin or a temp filename typically. Convert the journal field in config to
  a comment field.

### Added

- README
  - Instructions on install with bin.
  - Instructions on use as a formatter with Neovim etc.
- New doc `CONTRIBUTING.md`

## 0.0.0 - 2022-05-04

### Added

- Basic functionality complete.
