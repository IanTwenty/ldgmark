<!--
SPDX-FileCopyrightText: 2022-4 The authors <https://gitlab.com/IanTwenty/ldgmark>

SPDX-License-Identifier: CC-BY-SA-4.0

Assign ledger transactions to the right accounts automatically.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/ldgmark)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# ldgmark

![The word 'ldgmark' above a small pile of coins with a pencil next to them](ldgmark.png)

Command line tool to automatically assign [ledger](https://www.ledger-cli.org/)
transactions to the right accounts, based on config.

## Table of Contents

<!--toc:start-->

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [License](#license)

<!--toc:end-->

## Background

Part of importing transactions to ledger involves picking the right accounts for
each transaction. Ldgmark can scan a journal and assign the right accounts
based on the payee or a ledger [metadata tag](https://ledger-cli.org/doc/ledger3.html#metadata-tags).

Ldgmark is designed to work with other ledger import tools and specifically
it's sister [ldgclip](https://gitlab.com/IanTwenty/ldgclip) which can pull from
the clipboard.

It can also be used as a formatter with tools like Vim and Neovim to automatically
mark up your transactions on file save for example.

## Install

We only support Linux right now, more may be supported in future on request.

To install with [bin](https://github.com/marcosnils/bin):

```bash
bin install https://gitlab.com/IanTwenty/ldgmark
```

To install manually git clone this repo. Clone a tag to ensure you're on an
official version. Optionally copy `ldgmark` to a dir on your PATH, e.g.
`/usr/local/bin`.

## Usage

On first usage you need to configure ldgmark so it knows what assignments to
make or it will do nothing. Ldgmark expects a config file at
`$XDG_CONFIG_HOME/ldgmark/config` where each line is a set of fields separated
by pipes:

```ldgmark
COMMENT|PAYEE|1ST ACCOUNT NAME|2ND ACCOUNT NAME
```

Here's an example:

```ldgmark
Cinema|Everyman|Assets:Funds:Entertainment|Expenses:Cinema
```

This tells ldgmark that if we see
a payee called `Everyman` we'll assign the first account posting as
`Assets:Funds:Entertainment` and the second as `Expenses:Cinema`.
The line has a simple comment `Cinema` for our own reminder.

If we look at this config as a table it will be clearer:

| Comment | Payee or :tag: | 1st Account Name           | 2nd Account Name |
| ------- | -------------- | -------------------------- | ---------------- |
| Cinema  | Everyman       | Assets:Funds:Entertainment | Expenses:Cinema  |

Each field needs further explanation:

- **Comment**: Can be empty, for your own use.
- **Payee or :tag:**: Payees that match this regex will be selected.
  Alternatively specify a ledger metadata :TAG: between colons and
  if it's found in the comments of the transaction it will be selected.
- **1st Account Name**: the account name to use for the first posting on each
  transaction.
- **2nd Account Name**: the account name to use for the second posting on each
  transaction.

Earlier lines in the config take precedence over later lines.

Ldgmark will only alter transactions who have postings to 'Assets:Unallocated'
and 'Expenses:Unallocated'. This ensure it does not mess with transactions that
have been previously imported. For example this transaction will be considered:

```ledger
2019/05/24 * CARD PAYMENT TO EKOLOGIK LTD
    Assets:Unallocated    -£16.60
    Expenses:Unallocated
```

Now we have a config file we can use ldgmark. Ldgmark is an awk script and as
such it takes any arguments awk can process. However in the simplest form just
provide a journal file via stdin or as an argument:

```bash
ldgmark JOURNAL
```

It'll print the altered journal to stdout where it can be further processed or
redirected to another file. To send it straight back to the file it came from
use `sponge` from [moreutils](https://joeyh.name/code/moreutils/):

```bash
ldgmark JOURNAL | sponge JOURNAL
```

Ldgmark can also be used as a formatter in a text editor. For example to add it
to Neovim with the plugin [stevearc/conform.nvim](https://github.com/stevearc/conform.nvim)
you might use this lua snippet:

```lua
opts = {
  formatters_by_ft = {
    ledger = {
      "ldgmark",
    },
  },
  formatters = {
    ldgmark = {
      command = "ldgmark",
    },
  },
}
```

## Roadmap

In vague order of priority:

- I would like to be told what config entries are no longer matching anything so
  I can clean them out.
- Some trans are not distinguishable by payee where payee has a lot of account
  activity. Instead use payee and amount. Amount could even be a range for
  amounts that vary slightly.
- Allow configuration of the 'unallocated' accounts we search for
- Pulling in other data sources, e.g. email to deal with transactions that are
  not descriptive. Some PayPal transactions have little info and sometimes the
  email gives nothing more than an abstract company name. Then need to search
  for other emails from this company name at same time, or search for the amount
  around the time and find what other emails come up. Adding in the actual
  payee as some kind of metadata would be helpful.

## Contributing

This software is young and probably has lots of problems and gaps in the
documentation! Help us by submitting any
[issues](https://gitlab.com/IanTwenty/ldgmark/-/issues) or questions that occur
to you.

## License

We declare our licensing by following the [REUSE
specification](https://reuse.software/) - copies of applicable licenses are
stored in the `LICENSES` directory. Here is a summary:

- All source code is licensed under [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).
- If it's not executable, including the text when extracted from code, it is
  licensed under [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html).

For more accurate information, check individual files.

Ldgmark is free software: you can redistribute or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
